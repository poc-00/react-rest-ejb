package com.vendor.service.rest.controller;

import com.vendor.service.ejb.entity.Transaction;
import com.vendor.service.rest.request.RequestData;
import com.vendor.service.rest.response.ResponseData;
import com.vendor.service.rest.service.DataService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/data")
public class DataController {

    @Inject
    private DataService dataService;

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getMessage(@QueryParam("name") String name) {
        System.out.println(dataService);
        return dataService.getMessage(name);
    }

    /*CRUD OPERATION ON TRANSACTION TABLE*/

    @POST
    @Path("/create-transaction")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseData createTransaction(RequestData requestData) {
        ResponseData responseData = new ResponseData();
        try {
            System.out.println(requestData);
            responseData.setResponse(dataService.createTransaction(requestData.getData()));
        } catch (Exception e) {
            System.out.println(e.getMessage());
            responseData.setStatus("Not Saved");
            responseData.setError(e.getMessage());
            e.printStackTrace();
        } finally {
            System.out.println("Completed");
        }
        return responseData;
    }

    @PUT
    @Path("/update-transaction")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseData updateTransaction(RequestData requestData) {
        ResponseData responseData = new ResponseData();
        try {
            System.out.println(requestData);
            responseData.setResponse(dataService.updateTransaction(requestData.getData()));
        } catch (Exception e) {
            System.out.println(e.getMessage());
            responseData.setStatus("Not Saved");
            responseData.setError(e.getMessage());
            e.printStackTrace();
        } finally {
            System.out.println("Completed");
        }
        return responseData;
    }

    @PATCH
    @Path("/field-update-transaction")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseData fieldUpdateTransaction(RequestData requestData) {
        ResponseData responseData = new ResponseData();
        try {
            System.out.println(requestData);
            responseData.setResponse(dataService.fieldUpdateTransaction(requestData.getFields()));
        } catch (Exception e) {
            System.out.println(e.getMessage());
            responseData.setStatus("Not Saved");
            responseData.setError(e.getMessage());
            e.printStackTrace();
        } finally {
            System.out.println("Completed");
        }
        return responseData;
    }

    @DELETE
    @Path("/delete-transaction/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseData deleteTransaction(@PathParam("id") int id) {
        ResponseData responseData = new ResponseData();
        try {
            System.out.println("Id is "+id);
            boolean isDeleted = dataService.deleteTransaction(id);
            responseData.setStatus(isDeleted ? "Deleted" : "Not Deleted");
        } catch (Exception e) {
            System.out.println(e.getMessage());
            responseData.setStatus("Not Saved");
            responseData.setError(e.getMessage());
            e.printStackTrace();
        } finally {
            System.out.println("Completed");
        }
        return responseData;
    }

    @GET
    @Path("/transaction/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseData getTransaction(@PathParam("id") int id) {
        ResponseData responseData = new ResponseData();
        try {
            System.out.println("Id is "+id);
            Transaction transaction = dataService.getTransaction(id);
            responseData.setResponse(transaction);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            responseData.setStatus("Not Saved");
            responseData.setError(e.getMessage());
            e.printStackTrace();
        } finally {
            System.out.println("Completed");
        }
        return responseData;
    }

    @GET
    @Path("/all-transaction")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseData getAllTransaction() {
        ResponseData responseData = new ResponseData();
        try {
            System.out.println("Started");
            List transactions = dataService.getAllTransaction();
            responseData.setData(transactions);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            responseData.setStatus("Not Saved");
            responseData.setError(e.getMessage());
            e.printStackTrace();
        } finally {
            System.out.println("Completed");
        }
        return responseData;
    }

}
