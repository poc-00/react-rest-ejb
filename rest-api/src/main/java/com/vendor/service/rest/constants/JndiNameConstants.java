package com.vendor.service.rest.constants;

import com.vendor.service.ejb.remote.DataBeanRemote;

import javax.ejb.Singleton;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.util.Hashtable;

@Singleton
public class JndiNameConstants {

    /*
    java:app/rest-api-1.0-SNAPSHOT/dataEJB!com.vendor.service.ejb.local.DataBeanLocal
    java:module/dataEJB!com.vendor.service.ejb.local.DataBeanLocal
    ejb:/rest-api-1.0-SNAPSHOT/dataEJB!com.vendor.service.ejb.local.DataBeanLocal
    java:global/rest-api-1.0-SNAPSHOT/dataEJB!com.vendor.service.ejb.remote.DataBeanRemote
    java:app/rest-api-1.0-SNAPSHOT/dataEJB!com.vendor.service.ejb.remote.DataBeanRemote
    java:module/dataEJB!com.vendor.service.ejb.remote.DataBeanRemote
    java:jboss/exported/rest-api-1.0-SNAPSHOT/dataEJB!com.vendor.service.ejb.remote.DataBeanRemote
    ejb:/rest-api-1.0-SNAPSHOT/dataEJB!com.vendor.service.ejb.remote.DataBeanRemote
    */


    /*
    Not Working - will try later
    private static final Hashtable JNDI_PROPERTIES = new Hashtable() {{
        put(Context.INITIAL_CONTEXT_FACTORY, "org.wildfly.naming.client.WildFlyInitialContextFactory");
        put(Context.PROVIDER_URL, "http-remoting://localhost:8080");
    }};

    private Context context;

    public static final String DATA_BEAN_LOCAL = "ejb:/rest-api-1.0-SNAPSHOT/dataEJB!com.vendor.service.ejb.local.DataBeanLocal";
    public static final String DATA_BEAN_REMOTE = "ejb:/rest-api-1.0-SNAPSHOT/dataEJB!com.vendor.service.ejb.local.DataBeanRemote";

    public JndiNameConstants() throws NamingException {
        this.context = new InitialContext(JNDI_PROPERTIES);
    }

    public DataBeanRemote getDataBeanRemote() throws NamingException {
        return (DataBeanRemote) this.context.lookup(DATA_BEAN_REMOTE);
    }*/

    public DataSource getDataSource() throws NamingException {
        return (DataSource)InitialContext.doLookup("java:/VENDOR");
    }

}
