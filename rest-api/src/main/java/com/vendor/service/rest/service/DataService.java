package com.vendor.service.rest.service;

import com.vendor.service.ejb.entity.Transaction;

import java.util.List;
import java.util.Map;

public interface DataService {
    String getMessage(String name);
    Transaction createTransaction(Transaction transaction);
    Transaction updateTransaction(Transaction transaction);
    Transaction fieldUpdateTransaction(Map<String, Object> fields);
    boolean deleteTransaction(int id);
    Transaction getTransaction(int id);
    List<Transaction> getAllTransaction();
}
