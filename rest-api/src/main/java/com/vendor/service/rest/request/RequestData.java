package com.vendor.service.rest.request;

import com.vendor.service.ejb.entity.Transaction;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import java.util.Map;

@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class RequestData {
    Transaction data;
    Map<String,Object> fields;

    @Override
    public String toString() {
        return "RequestData{" +
                "data=" + data +
                ", fields=" + fields +
                '}';
    }
}
