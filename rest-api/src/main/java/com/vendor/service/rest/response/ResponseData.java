package com.vendor.service.rest.response;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.vendor.service.ejb.entity.Transaction;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class ResponseData {
    List<Transaction> data;
    Transaction response;
    String status;
    String error;
}
