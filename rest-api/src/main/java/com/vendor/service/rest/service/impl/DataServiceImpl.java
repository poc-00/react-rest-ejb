package com.vendor.service.rest.service.impl;

import com.vendor.service.ejb.entity.Transaction;
import com.vendor.service.ejb.remote.DataBeanRemote;
import com.vendor.service.rest.service.DataService;

import javax.ejb.EJB;
import javax.ejb.Singleton;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Singleton
public class DataServiceImpl implements DataService {

    @EJB(lookup = "java:global/ejb/dataEJB!com.vendor.service.ejb.remote.DataBeanRemote")
    private DataBeanRemote dataBeanRemote;

    @Override
    public String getMessage(String name) {
        return dataBeanRemote.getMessage(name);
    }

    @Override
    public Transaction createTransaction(Transaction transaction) {
        transaction.setCreatedAt(new Date());
        Transaction tr = dataBeanRemote.createTransaction(transaction);
        return tr;
    }

    @Override
    public Transaction updateTransaction(Transaction transaction) {
        transaction.setUpdatedAt(new Date());
        Transaction tr = dataBeanRemote.updateTransaction(transaction);
        return tr;
    }

    @Override
    public Transaction fieldUpdateTransaction(Map<String, Object> fields) {
        fields.put("updatedAt", new Date());
        Transaction tr = dataBeanRemote.fieldUpdateTransaction(fields);
        return tr;
    }

    @Override
    public boolean deleteTransaction(int id) {
        boolean isDeleted = dataBeanRemote.deleteTransaction(id);
        return isDeleted;
    }

    @Override
    public Transaction getTransaction(int id) {
        return dataBeanRemote.getTransaction(id);
    }

    public List getAllTransaction() {
        return dataBeanRemote.getAllTransaction();
    }

}
