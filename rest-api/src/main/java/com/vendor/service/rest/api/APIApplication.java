package com.vendor.service.rest.api;

import com.vendor.service.rest.controller.DataController;
import com.vendor.service.rest.filter.CorsFilter;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

@ApplicationPath("/api")
public class APIApplication extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> set = new HashSet<>();
        set.add(CorsFilter.class);

        set.add(DataController.class);
        return set;
    }
}
