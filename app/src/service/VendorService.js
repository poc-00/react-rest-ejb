import Constants from "../constants/Constants";

class VendorService {
    constructor() {
        let constants = new Constants();
        this.url = constants.URL + constants.CONTEXT + constants.VENDOR_PATH;
    }

    async createTransaction(data) {
        console.log(this.url);
        return fetch(this.url + "/create-transaction", {
            method: 'post',// get | put | patch | delete
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data)
        }).then(response => {
            return response.json();
        }).catch(reason => {
            return reason;
        }).finally(() => console.log("Api call completed"));;

    }

}

export default VendorService;
