import React, {Component} from 'react';
import './App.css';
import './css/vedorform.css';
import MainTitle from './component/MainTitle';
import VendorForm from './component/VendorForm';

class App extends Component {
    render() {
        return (
            <div>
                <MainTitle/>
                <VendorForm/>
            </div>
        )
    }
}

export default App;
