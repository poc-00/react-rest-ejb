class Constants {
    URL = "http://localhost:8080";
    CONTEXT = "/rest-api/api";
    VENDOR_PATH = '/data';
    CREATE_TRANSACTION = '/create-transaction';
}

export default Constants;
