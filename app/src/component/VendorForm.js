import React from 'react';
import VendorService from "../service/VendorService";

class VendorForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            description: ''
        };
        this.vendorService = new VendorService();
        this.handleChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
        console.log('Change detected. State updated' + name + ' = ' + value);
    }

    handleSubmit(event) {
        let data = {'data': {'name': this.state.name, 'description': this.state.description, 'createdUser': 'UIYDRR'}};
        this.vendorService.createTransaction(data).then(response => {
            console.log(response);
            alert("Data Persisted");
        }).catch(reason => {
            console.log(reason);
            alert("Data Not Persisted");
        }).finally(() => console.log("Api call completed"));
        event.preventDefault();
    }

    render() {
        return (
            <div>
                <form onSubmit={this.handleSubmit}>
                    <div className="form-group">
                        <label htmlFor="nameInput">Vendor Name</label>
                        <input type="text" name="name" value={this.state.name} onChange={this.handleChange}
                               className="form-control" id="nameInput" placeholder="Name"/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="descriptionInput">Description</label>
                        <input name="description" type="text" value={this.state.description}
                               onChange={this.handleChange}
                               className="form-control" id="descriptionInput" placeholder="Vendor Description"/>
                    </div>
                    <input type="submit" value="Submit" className="btn btn-primary"/>
                </form>
            </div>
        )
    }
}

export default VendorForm;
