package com.vendor.service.ejb.remote;

import com.vendor.service.ejb.entity.Transaction;

import javax.ejb.Remote;
import java.util.List;
import java.util.Map;

@Remote
public interface DataBeanRemote {
    String getMessage(String name);
    Transaction createTransaction(Transaction transaction);
    Transaction updateTransaction(Transaction transaction);
    Transaction fieldUpdateTransaction(Map<String, Object> fields);
    boolean deleteTransaction(int id);
    Transaction getTransaction(int id);
    List getAllTransaction();
}
