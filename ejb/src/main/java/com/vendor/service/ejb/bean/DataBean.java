package com.vendor.service.ejb.bean;

import com.vendor.service.ejb.entity.Transaction;
import com.vendor.service.ejb.local.DataBeanLocal;
import com.vendor.service.ejb.remote.DataBeanRemote;

import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;

@Stateless(name = "dataEJB")
public class DataBean implements DataBeanLocal, DataBeanRemote {

    private final String[] messages = {"Hey", "Hi", "Hello"};

    public String getMessage(String name) {
        return String.format("%s %s!", messages[new Random().nextInt(3)], name);
    }

    @PersistenceContext(unitName = "TransactionPU")
    private EntityManager entityManager;

    @Resource(lookup = "java:/VENDOR")
    private DataSource dataSource;

    @Override
    public Transaction createTransaction(Transaction transaction) {
        try {
            System.out.println("Started");
            entityManager.persist(transaction);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            System.out.println("Completed");
        }
        return transaction;
    }

    @Override
    public Transaction updateTransaction(Transaction transaction) {
        try {
            System.out.println("Started");
            entityManager.merge(transaction);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            System.out.println("Completed");
        }
        return transaction;
    }

    @Override
    public Transaction fieldUpdateTransaction(Map<String, Object> fields) {
        try {
            System.out.println("Started");
            Transaction tr = entityManager.find(Transaction.class, Integer.parseInt(fields.get("id").toString()));
            fields.forEach((k,v) -> {
                if (k.equals("name")) {
                    tr.setName(v.toString());
                }
                if (k.equals("description")) {
                    tr.setDescription(v.toString());
                }
                if (k.equals("updatedUser")) {
                    tr.setUpdatedUser(v.toString());
                }
                tr.setUpdatedAt(new Date());
            });
            Transaction result = entityManager.merge(tr);
            return result;
        }catch (Exception e) {
            e.printStackTrace();
        }finally {
            System.out.println("Completed");
        }
        return null;
    }

    @Override
    public boolean deleteTransaction(int id) {
        try {
            System.out.println("Id is in Bean "+id);
            Transaction tr = entityManager.find(Transaction.class, id);
            if (tr != null) {
                entityManager.remove(tr);
                return true;
            }else {
                System.out.println("The data is not available for id "+id);
            }
        }catch (Exception e) {
            e.printStackTrace();
        }finally {
            System.out.println("Completed");
        }
        return false;
    }

    @Override
    public Transaction getTransaction(int id) {
        try {
            System.out.println("Started");
            Transaction tr = entityManager.find(Transaction.class, id);
            if (tr != null) {
                return tr;
            }else {
                System.out.println("The data is not available for id "+id);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            System.out.println("Completed");
        }
        return null;
    }

    @Override
    public List getAllTransaction() {
        return entityManager.createNativeQuery("select * from VEN.TRANSACTION", Transaction.class). getResultList();
    }
}
