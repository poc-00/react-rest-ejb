package com.vendor.service.ejb.local;

import com.vendor.service.ejb.entity.Transaction;

import javax.ejb.Local;
import java.util.List;
import java.util.Map;

@Local
public interface DataBeanLocal {
    String getMessage(String name);
    Transaction createTransaction(Transaction transaction);
    Transaction updateTransaction(Transaction transaction);
    Transaction fieldUpdateTransaction(Map<String, Object> fields);
    boolean deleteTransaction(int id);
    Transaction getTransaction(int id);
    List getAllTransaction();
}
